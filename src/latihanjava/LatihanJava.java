/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package latihanjava;
import java.util.Arrays;
import java.util.Collections; 

/**
 *
 * @author Djaya
 */
public class LatihanJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     
        //set array
        String[] array= {"Dancer 1 - Bella, Nathania, Yohanna", "Dancer 2 - Andrew, Michael, Jason", "Dancer 3 - Megan, Riana, Nicole"};
        
        for(int i=0; i < array.length; i++) {
            //split -
            String[] split = array[i].split("-");
            
            //set variabel value
            String dancer = split[0];
            String namas = split[1]; 
            
            //split ,
            String[] splitnama = namas.split(",");

            // Sort A-Z 
            Arrays.sort(splitnama);

            
            System.out.println(dancer + ":");
            System.out.println(Arrays.toString(splitnama));
        }

    }
    
}
